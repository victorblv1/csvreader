package com.viktorsb;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {

        String urlString = "https://people.sc.fsu.edu/~jburkardt/data/csv/oscar_age_female.csv";

        // create the url
        URL url = new URL(urlString);

        /**
         * This part gather the csv date from the given url
         */
//        // open the url stream, wrap it an a few "readers"
//        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
//
//        // write the output to stdout
//        String line;
//        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
//        }
//        // close our reader
//        reader.close();


        /**
         * This part not only gather the csv data from the given url
         * but also convert it into an array
         */
        List<String> content = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                content.add(line);
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        System.out.println(content.get(1));    // prints value under the index ()
        System.out.println(content);    // prints all
    }
}
